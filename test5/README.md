# 实验5：包，过程，函数的用法

姓名:庞子翔 学号：202010414413

## 实验目的

了解PL/SQL语言结构
了解PL/SQL变量和常量的声明和使用方法
学习包，过程，函数的用法

## 实验内容


```python
以hr用户登录
创建一个包(Package)，包名是MyPack。
在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：
```


```python
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID

```


```python
create or replace PACKAGE MyPack IS

FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/

```

![](创建.png)


```python
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;


```

![](测试.png)


```python
过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;

```

![](测试2.png)

PL/SQL是Oracle数据库中的编程语言，它允许您编写存储在数据库中的程序。以下是一些PL/SQL语言结构：

声明部分：在此部分声明变量和常量。
执行部分：在此部分编写实际的PL/SQL代码。
异常部分：在此部分处理异常和错误。
包、过程和函数是PL/SQL中的三种程序单元。以下是它们的用法：

包：包是一个逻辑单元，可以包含变量、常量、类型定义和子程序等内容。包通常用于将相关的PL/SQL代码组织在一起以便于管理和重用。
过程：过程是一种命名的PL/SQL块，可以接受输入参数，执行一些操作并返回输出参数。过程通常用于执行重复的任务，例如更新记录或计算结果。
函数：函数是一种命名的PL/SQL块，可以接受输入参数，执行一些操作并返回一个值。函数通常用于计算值，例如在SQL查询中使用。


```python

```
