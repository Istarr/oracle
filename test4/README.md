# 实验4：PL/SQL语言打印杨辉三角

姓名：庞子翔 学号：202010414413

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

认真阅读并运行下面的杨辉三角源代码。
将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
运行这个存储过程即可以打印出N行杨辉三角。
写出创建YHTriange的SQL语句。

## 杨辉三角源代码


```python
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/

```


```python
代码使用了一个数组 rowArray，该数组存储杨辉三角中每一行的数字，并在每一行结束后打印出来。下面是代码的详细分析：

type t_number is varray (100) of integer not null;
定义了一个类型 t_number，该类型是一个大小为 100 的整数数组。

spaces varchar2(30) :=' ';
定义了一个变量 spaces，用于在打印数字时分隔每一个数字。

N integer := 9;
定义了一个变量 N，指定了杨辉三角形的行数。

rowArray t_number := t_number();
声明并初始化了一个 t_number 类型的数组 rowArray。

dbms_output.put_line('1');
先输出杨辉三角形的第一行。

dbms_output.put(rpad(1,9,' '));
先输出杨辉三角形的第二行的第一个数字（数字 1）。

dbms_output.put(rpad(1,9,' '));
先输出杨辉三角形的第二行的第二个数字（数字 1）。

dbms_output.put_line('');
输出一个换行符，换行到下一行。

for i in 1 .. N loop rowArray.extend; end loop;
用 extend 方法初始化 rowArray 数组的大小为 N，每个元素的值为 0。

rowArray(1):=1; rowArray(2):=1;
将 rowArray 数组的第一个和第二个元素赋值为 1。

for i in 3 .. N loop
从第三行开始循环每一行。

rowArray(i):=1;
将当前行的最后一个数字赋值为 1。

while j>1 loop rowArray(j):=rowArray(j)+rowArray(j-1); j:=j-1; end loop;
计算当前行的每个数字的值，从第二个数字开始到倒数第二个数字，每个数字的值为其上一行相邻两个数字之和。

for j in 1 .. i loop dbms_output.put(rpad(rowArray(j),9,' ')); end loop;
打印当前行的所有数字，使用 rpad 函数在数字前面填充空格，保证每个数字占据相同的宽度。

dbms_output.put_line('');
打印一个换行符，换行到下一行。

总之，这段代码使用数组和循环语句计算和打印了一个杨辉三角形。
```

## 建立存储代码

CREATE OR REPLACE PROCEDURE YANGHUI(N IN integer)
AS
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;

![](1_1.png)

![](2_2.png)

在oracle中使用存储功能（如存储过程、函数、触发器等）有以下优势：

代码复用：通过编写存储过程、函数等，可以将一段特定的功能封装在一个独立的模块中，可以在程序的任何地方调用这个模块，从而减少了代码的重复，提高了代码的可维护性和可扩展性。

安全性：存储功能可以被授权给特定的用户或角色，从而实现对数据库的访问控制，确保数据的安全性。

性能：存储过程和函数可以在数据库服务器上执行，减少了数据传输的次数，可以提高数据库的性能。

数据完整性：存储过程可以实现一些数据完整性约束，如实现唯一性约束、外键约束等，可以确保数据的完整性。

降低耦合性：存储功能可以将业务逻辑和数据库操作分离，从而降低了应用程序和数据库之间的耦合度，使得应用程序更容易维护和升级。

综上所述，存储功能的使用可以提高代码的复用性、安全性、性能、数据完整性和可维护性，是数据库应用开发中的一个重要工具。
