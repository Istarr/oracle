﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

# 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。

在数据库中创建表空间需要在终端中创建一个用户，再讲权限赋予给用户

在 Oracle 数据库中创建表空间的代码：
```sql
sql
Copy code
-- 以 SYSDBA 或 SYSOPER 角色登录

-- 创建数据表空间
CREATE TABLESPACE data_ts
  DATAFILE 'data_ts.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 10M
  MAXSIZE UNLIMITED;

-- 创建索引表空间
CREATE TABLESPACE index_ts
  DATAFILE 'index_ts.dbf'
  SIZE 50M
  AUTOEXTEND ON
  NEXT 10M
  MAXSIZE UNLIMITED;

-- 授予表空间权限给 istarr 用户
ALTER USER istarr QUOTA UNLIMITED ON data_ts;
ALTER USER istarr QUOTA UNLIMITED ON index_ts;
```
![](创建数据库用户.png)

进行表空间的创建
```sql
CREATE TABLESPACE data_ts
  DATAFILE 'data_ts.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 10M
  6  MAXSIZE UNLIMITED;

表空间已创建。

CREATE TABLESPACE index_ts
  DATAFILE 'index_ts.dbf'
  SIZE 50M
  AUTOEXTEND ON
  NEXT 10M
  6    MAXSIZE UNLIMITED;

表空间已创建。


```
![](创建用户2.png)


创建数据库连接‘work’，对数据库进行创建表，创建商品表，客户表，订单表，销售记录表。

商品表（products）:

列：id（编号，数字类型）、name（名称，最大长度为100的字符串类型）、price（价格，最大长度为10，保留2位小数的数字类型）、category（类别，最大长度为50的字符串类型）。
主键：id。
客户表（customers）:

列：id（编号，数字类型）、name（名称，最大长度为100的字符串类型）、address（地址，最大长度为200的字符串类型）、phone（电话，最大长度为20的字符串类型）、credit_lim（信用额度，最大长度为10，保留2位小数的数字类型）。
主键：id。
订单表（orders）:

列：id（编号，数字类型）、customer_id（客户编号，数字类型）、order_date（订单日期，日期类型）、total（总金额，最大长度为10，保留2位小数的数字类型）。
主键：id。
外键：customer_id（引用自customers表的id列）。
销售记录表（sales_records）:

列：id（编号，数字类型）、order_id（订单编号，数字类型）、product_id（商品编号，数字类型）、quantity（数量，数字类型）。
主键：id。
外键：order_id（引用自orders表的id列）、product_id（引用自products表的id列）。
以上是表结构的基本信息。这些表之间存在一些关系：

products表和sales_records表之间是一对多的关系，即一个商品可以在多个销售记录中出现。
customers表和orders表之间是一对多的关系，即一个客户可以有多个订单。
orders表和sales_records表之间是一对多的关系，即一个订单可以有多个销售记录。
这样的表结构可以用于记录商品、客户、订单和销售记录的信息，并通过外键建立它们之间的关联关系。
```sql

-- 创建商品表
CREATE TABLE products (
  id       NUMBER,
  name     VARCHAR2(100),
  price    NUMBER(10, 2),
  category VARCHAR2(50),
  PRIMARY KEY (id)
) TABLESPACE data_ts;

-- 创建客户表
CREATE TABLE customers (
  id         NUMBER,
  name       VARCHAR2(100),
  address    VARCHAR2(200),
  phone      VARCHAR2(20),
  credit_lim NUMBER(10, 2),
  PRIMARY KEY (id)
) TABLESPACE data_ts;

-- 创建订单表
CREATE TABLE orders (
  id          NUMBER,
  customer_id NUMBER,
  order_date  DATE,
  total       NUMBER(10, 2),
  PRIMARY KEY (id),
  FOREIGN KEY (customer_id) REFERENCES customers (id)
) TABLESPACE data_ts;

-- 创建销售记录表
CREATE TABLE sales_records (
  id         NUMBER,
  order_id   NUMBER,
  product_id NUMBER,
  quantity   NUMBER,
  PRIMARY KEY (id),
  FOREIGN KEY (order_id) REFERENCES orders (id),
  FOREIGN KEY (product_id) REFERENCES products (id)
) TABLESPACE data_ts;
```
给这几个表插入十万条虚拟数据，对表进行平均分配。
```sql

-- 插入2.5万条客户数据
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO customers (id, name, address, phone, credit_lim)
    VALUES (i, 'Customer ' || i, 'Address ' || i, 'Phone ' || i, ROUND(DBMS_RANDOM.VALUE(1000, 10000), 2));
  END LOOP;
  COMMIT;
END;
/

-- 插入2.5万条商品数据
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO products (id, name, price, category)
    VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 1000), 2), 'Category ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 插入2.5万条订单数据
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO orders (id, customer_id, order_date, total)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 25000)), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365)), ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
  END LOOP;
  COMMIT;
END;
/

-- 插入2.5万条销售记录数据
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO sales_records (id, order_id, product_id, quantity)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 25000)), ROUND(DBMS_RANDOM.VALUE(1, 25000)), ROUND(DBMS_RANDOM.VALUE(1, 10)));
  END LOOP;
  COMMIT;
END;
/
```

# 设计权限及用户分配方案。至少两个用户。
给istarr用户授予权限，并创建两个用户,对这两个用户进行权限授予
```sql


ALTER USER istarr QUOTA UNLIMITED ON data_ts;

用户已更改。

SQL> ALTER USER istarr QUOTA UNLIMITED ON index_ts;

用户已更改。

SQL> CREATE USER sales_user IDENTIFIED BY 123;

用户已创建。

SQL> GRANT CONNECT, RESOURCE, CREATE SESSION TO sales_user;

授权成功。

SQL> ALTER USER sales_user QUOTA UNLIMITED ON data_ts;

用户已更改。

SQL> ALTER USER sales_user QUOTA UNLIMITED ON index_ts;

用户已更改。

```
![](创建用户.png)
![](用户权限.png)


# 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
建立Oracle存储过程有以下几个主要原因：

提高性能：存储过程在数据库中进行预编译和优化，可以提高查询和处理数据的性能。一旦存储过程被编译和优化，它们可以重复使用，减少了每次执行时的开销。

代码重用：存储过程可以被多个应用程序或模块共享和调用，实现代码的重用。这减少了重复编写相同代码的工作量，并且可以确保应用程序在不同的地方使用相同的业务逻辑。

数据库逻辑封装：存储过程将复杂的数据库逻辑封装在一个单独的单元中。这样，客户端应用程序只需要调用存储过程，而不需要了解底层的逻辑细节。这提高了应用程序的可维护性，并且可以在不影响应用程序代码的情况下对底层逻辑进行修改和优化。

数据库安全性：通过存储过程，可以对数据库中的敏感操作进行控制和限制。存储过程可以实施访问控制策略，只允许授权用户执行特定的操作。这样可以确保数据库的安全性，并且防止未经授权的访问和修改数据。
下面在work数据库中建立一个存储过程存储来了5个查询语句

```sql


CREATE OR REPLACE PACKAGE sales_package AS
  -- 存储过程：计算指定客户的购买历史
  PROCEDURE get_customer_purchase_history(
    customer_id IN NUMBER,
    purchase_history OUT SYS_REFCURSOR
  );

  -- 存储过程：获取最畅销的产品
  PROCEDURE get_best_selling_product(
    best_selling_product OUT SYS_REFCURSOR
  );

  -- 存储过程：计算每个客户的总购买金额
  PROCEDURE get_total_purchase_amount_per_customer(
    total_purchase_amount OUT SYS_REFCURSOR
  );

  -- 存储过程：计算指定产品的销售数量
  PROCEDURE get_product_sales_quantity(
    product_id IN NUMBER,
    sales_quantity OUT NUMBER
  );

  -- 存储过程：计算每个客户的平均购买金额
  PROCEDURE get_average_purchase_amount_per_customer(
    average_purchase_amount OUT SYS_REFCURSOR
  );
END sales_package;
/

CREATE OR REPLACE PACKAGE BODY sales_package AS
  -- 存储过程：计算指定客户的购买历史
  PROCEDURE get_customer_purchase_history(
    customer_id IN NUMBER,
    purchase_history OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN purchase_history FOR
      SELECT o.id, p.name, s.quantity, p.price, s.quantity * p.price AS total_amount
      FROM orders o
      JOIN sales_records s ON o.id = s.order_id
      JOIN products p ON s.product_id = p.id
      WHERE o.customer_id = customer_id;
  END get_customer_purchase_history;

  -- 存储过程：获取最畅销的产品
  PROCEDURE get_best_selling_product(
    best_selling_product OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN best_selling_product FOR
      SELECT p.id, p.name, SUM(s.quantity) AS total_quantity
      FROM sales_records s
      JOIN products p ON s.product_id = p.id
      GROUP BY p.id, p.name
      ORDER BY total_quantity DESC
      FETCH FIRST ROW ONLY;
  END get_best_selling_product;

  -- 存储过程：计算每个客户的总购买金额
  PROCEDURE get_total_purchase_amount_per_customer(
    total_purchase_amount OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN total_purchase_amount FOR
      SELECT c.id, c.name, SUM(s.quantity * p.price) AS total_amount
      FROM customers c
      JOIN orders o ON c.id = o.customer_id
      JOIN sales_records s ON o.id = s.order_id
      JOIN products p ON s.product_id = p.id
      GROUP BY c.id, c.name;
  END get_total_purchase_amount_per_customer;

  -- 存储过程：计算指定产品的销售数量
  PROCEDURE get_product_sales_quantity(
    product_id IN NUMBER,
    sales_quantity OUT NUMBER
  ) AS
  BEGIN
    SELECT SUM(quantity)
    INTO sales_quantity
    FROM sales_records
    WHERE product_id = product_id;
  END get_product_sales_quantity;

  -- 存储过程：计算每个客户的平均购买金额
  PROCEDURE get_average_purchase_amount_per_customer(
    average_purchase_amount OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN average_purchase_amount FOR
      SELECT c.id, c.name, AVG(s.quantity * p.price) AS average_amount
      FROM customers c
      JOIN orders o ON c.id = o.customer_id
      JOIN sales_records s ON o.id = s.order_id
      JOIN products p ON s.product_id = p.id
      GROUP BY c.id, c.name;
  END get_average_purchase_amount_per_customer;
END sales_package;
/

```
存储语句的执行

```sql

-- 示例调用存储过程 get_best_selling_product
DECLARE
  best_selling_product SYS_REFCURSOR;
  product_id NUMBER;
  product_name VARCHAR2(100);
  total_quantity NUMBER;
BEGIN
  sales_package.get_best_selling_product(best_selling_product => best_selling_product);
  FETCH best_selling_product INTO product_id, product_name, total_quantity;
  IF best_selling_product%FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Best Selling Product: ' || product_name || ', Total Quantity: ' || total_quantity);
  END IF;
  CLOSE best_selling_product;
END;
/

-- 示例调用存储过程 get_total_purchase_amount_per_customer
DECLARE
  total_purchase_amount SYS_REFCURSOR;
  customer_id NUMBER;
  customer_name VARCHAR2(100);
  total_amount NUMBER;
BEGIN
  sales_package.get_total_purchase_amount_per_customer(total_purchase_amount => total_purchase_amount);
  LOOP
    FETCH total_purchase_amount INTO customer_id, customer_name, total_amount;
    EXIT WHEN total_purchase_amount%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('Customer ID: ' || customer_id || ', Customer Name: ' || customer_name || ', Total Purchase Amount: ' || total_amount);
  END LOOP;
  CLOSE total_purchase_amount;
END;
/



DECLARE
  purchase_history SYS_REFCURSOR;
  order_id NUMBER;
  product_name VARCHAR2(100);
  quantity NUMBER;
  price NUMBER;
  total_amount NUMBER;
  counter NUMBER := 0; -- 计数器
BEGIN
  sales_package.get_customer_purchase_history(customer_id => 1, purchase_history => purchase_history);
  LOOP
    FETCH purchase_history INTO order_id, product_name, quantity, price, total_amount;
    EXIT WHEN purchase_history%NOTFOUND OR counter >= 100; -- 添加计数器判断条件
    counter := counter + 1; -- 每次循环计数器加1
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || order_id || ', Product: ' || product_name || ', Quantity: ' || quantity || ', Price: ' || price || ', Total Amount: ' || total_amount);
  END LOOP;
  CLOSE purchase_history;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLERRM);
END;
/


DECLARE
  product_id NUMBER := 1; -- 替换为实际的产品ID
  sales_quantity NUMBER;
BEGIN
  sales_package.get_product_sales_quantity(product_id => product_id, sales_quantity => sales_quantity);
  DBMS_OUTPUT.PUT_LINE('Sales Quantity for Product ID ' || product_id || ': ' || sales_quantity);
END;



DECLARE
  average_purchase_amount SYS_REFCURSOR;
  customer_id NUMBER;
  customer_name VARCHAR2(100);
  average_amount NUMBER;
BEGIN
  sales_package.get_average_purchase_amount_per_customer(average_purchase_amount => average_purchase_amount);
  LOOP
    FETCH average_purchase_amount INTO customer_id, customer_name, average_amount;
    EXIT WHEN average_purchase_amount%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('Customer ID: ' || customer_id || ', Name: ' || customer_name || ', Average Purchase Amount: ' || average_amount);
  END LOOP;
  CLOSE average_purchase_amount;
END;
```
![](存储过程.png)
![](查询语句.png)

存储过程：get_customer_purchase_history

参数：customer_id（输入参数，客户编号），purchase_history（输出参数，结果集）
功能：根据指定的客户编号，获取该客户的购买历史。查询结果包括订单编号（o.id）、产品名称（p.name）、销售数量（s.quantity）、单价（p.price）和总金额（s.quantity * p.price）。
查询语句：使用了多个表的连接操作（orders、sales_records和products），通过客户编号关联相关的数据，并返回结果集。
存储过程：get_best_selling_product

参数：best_selling_product（输出参数，结果集）
功能：获取最畅销的产品。查询结果包括产品编号（p.id）、产品名称（p.name）和销售数量的总和（SUM(s.quantity)）。
查询语句：使用了多个表的连接操作（sales_records和products），通过产品编号关联相关的数据，并按销售数量的总和进行降序排序，最后只返回第一行结果。
存储过程：get_total_purchase_amount_per_customer

参数：total_purchase_amount（输出参数，结果集）
功能：计算每个客户的总购买金额。查询结果包括客户编号（c.id）、客户名称（c.name）和购买金额的总和（SUM(s.quantity * p.price)）。
查询语句：使用了多个表的连接操作（customers、orders、sales_records和products），通过客户编号关联相关的数据，并按客户分组计算购买金额的总和。
存储过程：get_product_sales_quantity

参数：product_id（输入参数，产品编号），sales_quantity（输出参数，销售数量）
功能：计算指定产品的销售数量。查询结果为单个值，即销售数量的总和（SUM(quantity)）。
查询语句：在sales_records表中根据产品编号进行过滤，计算销售数量的总和，并将结果赋值给输出参数sales_quantity。
存储过程：get_average_purchase_amount_per_customer

参数：average_purchase_amount（输出参数，结果集）
功能：计算每个客户的平均购买金额。查询结果包括客户编号（c.id）、客户名称（c.name）和购买金额的平均值（AVG(s.quantity * p.price)）。
查询语句：使用了多个表的连接操作（customers、orders、sales_records和products），通过客户编号关联相关的数据，并按客户分组计算购买金额的平均值。
这些存储过程提供了一些常见的数据查询和计算功能，以便在应用程序中使用。通过传递参数并执行这些存储过

# 设计一套数据库的备份方案。
完全备份是指备份整个数据库的过程，包括数据文件、控制文件、归档日志和参数文件等。它是数据库的一个镜像副本，可以用于完全恢复数据库到备份时的状态。在备份过程中，可以使用不同的工具和技术，如RMAN（Recovery Manager）来执行完全备份操作。完全备份通常用于创建数据库的初始备份，或者作为定期备份策略的一部分，以确保数据库的完整性和可恢复性。
本次设计中，使用完全备份方法。
```SQL
$ sqlplus / as sysdba
SQL> SHUTDOWN IMMEDIATE
SQL> STARTUP MOUNT
SQL> ALTER DATABASE ARCHIVELOG;
SQL> ALTER DATABASE OPEN;

$ rman target /
RMAN> SHOW ALL;
RMAN> BACKUP DATABASE;
RMAN> LIST BACKUP;
```

![](完全备份1.png)
![](完全备份2.png)
![](完全备份3.png)

# 总结
在这次Oracle实验中，我们涉及了许多重要的概念和操作，包括数据库的创建、表的设计、程序包的编写、权限管理和数据库备份等。以下是一些使用总结和体会：

数据库创建和表设计：在Oracle中，我们学习了如何创建数据库和设计表结构。合理的表设计可以提高数据库的性能和可扩展性，并满足实际业务需求。

程序包和存储过程：通过创建程序包和存储过程，我们可以封装复杂的逻辑和查询，提供更高层次的抽象和封装，方便数据库操作和应用开发。

权限管理：Oracle提供了灵活的权限管理机制，我们学习了如何创建用户、授予角色和权限，并且通过角色分配实现了权限的管理和控制。

多表联合查询：在程序包中，我们使用多表联合查询的方式检索数据，实现了对多个表的联合操作和数据提取。

数据库备份：备份是数据库管理中的重要任务之一。我们学习了使用RMAN进行数据库的完整备份，确保数据库的数据可以在发生故障时进行恢复。

通过这次实验，我们对Oracle数据库的基本操作和管理有了更深入的了解。我们学习了如何设计数据库结构、编写存储过程和程序包、管理用户和权限，并掌握了数据库备份的基本方法。这些知识和技能对于日后的数据库管理和开发工作都非常重要。同时，实验中的问题和错误也提醒我们在实际操作中要仔细阅读文档、注意细节，并不断提升自己的技术水平。

