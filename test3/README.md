# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
orders表按订单日期（order_date）设置范围分区。
order_details表设置引用分区。
表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
进行分区与不分区的对比实验。


```python
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```

![](1.png)

![](1.png)

该代码是一个 Oracle 数据库的 SQL 建表和分区语句。它定义了一个名为 orders 的表，其中包含多个字段和分区策略。

以下是该代码的详细分析：

表名为 orders，其中包含 8 个字段：
order_id：类型为 NUMBER，长度为 9，表示订单 ID。
customer_name：类型为 VARCHAR2，长度为 40 字节，表示客户名称。
customer_tel：类型为 VARCHAR2，长度为 40 字节，表示客户电话号码。
order_date：类型为 DATE，表示订单日期。
employee_id：类型为 NUMBER，长度为 6，表示员工 ID。
discount：类型为 NUMBER，长度为 8，小数位为 2，表示折扣，默认值为 0。
trade_receivable：类型为 NUMBER，长度为 8，小数位为 2，表示应收款，默认值为 0。
CONSTRAINT ORDERS_PK PRIMARY KEY：该语句定义了一个名为 ORDERS_PK 的主键约束，将 ORDER_ID 字段作为主键。
TABLESPACE USERS 表示该表将存储在 USERS 表空间中。

PCTFREE 10 INITRANS 1 表示每个数据块的空闲空间百分比为 10，初始事务槽数为 1。

STORAGE (BUFFER_POOL DEFAULT) 表示使用默认缓存池的存储选项。

PARTITION BY RANGE (order_date) 表示该表将按照订单日期进行分区。

接下来的语句定义了三个分区：

PARTITION PARTITION_BEFORE_2016：表示订单日期在 2016 年之前的订单将存储在该分区中。
PARTITION PARTITION_BEFORE_2020：表示订单日期在 2020 年之前的订单将存储在该分区中。
PARTITION PARTITION_BEFORE_2021：表示订单日期在 2021 年之前的订单将存储在该分区中。
对于每个分区，都定义了相应的存储选项、表空间、日志记录选项等。其中，第一个分区的存储选项中包含了 MINEXTENTS 1 MAXEXTENTS UNLIMITED，表示该分区的数据块可以动态增加。

最后一条语句表示，当需要存储 2022 年及以后的订单时，将增加名为 partition_before_2022 的新分区。

总体来说，这段代码建立了一个按订单日期分区的订单表，使得数据库可以更高效地管理和查询数据。


```python
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);

```

![](2.png)

上面的代码创建了一个名为"order_details"的表，包括以下列：

"id": 类型为NUMBER，精度为9，小数位为0的主键列。
"order_id": 类型为NUMBER，精度为10，小数位为0的外键列，参照了"orders"表中的"order_id"列。
"product_id": 类型为VARCHAR2，最大长度为40个字节，表示产品的ID。
"product_num": 类型为NUMBER，精度为8，小数位为2的列，表示产品数量。
"product_price": 类型为NUMBER，精度为8，小数位为2的列，表示产品价格。
此外，该表使用了基于引用关系的分区方式（PARTITION BY REFERENCE），也就是将其分区与所参照的表（orders）进行关联。


```python
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;

```

上面的代码创建了一个名为"SEQ1"的序列，具有以下特性：

最小值为1，最大值为999999999
每次递增量为1
起始值为1
使用缓存区，每次缓存20个值以提高性能
不保证顺序（NOORDER）
不循环（NOCYCLE）
不保留（NOKEEP）
不进行缩放（NOSCALE）
全局可见性（GLOBAL），即可供所有会话使用。


```python
DECLARE
  i INTEGER;
  y INTEGER;
  m INTEGER;
  d INTEGER;
  str VARCHAR2(100);
  max_order_id NUMBER;
BEGIN
  SELECT MAX(order_id) INTO max_order_id FROM orders;
  IF max_order_id IS NULL THEN
    max_order_id := 0;
  END IF;

  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
      m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(max_order_id+i,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
```

![](3.png)

上面的代码是一个PL/SQL语句块，包含以下步骤：

声明变量i、y、m、d和str，以及max_order_id变量用于存储orders表中最大的order_id。
执行一个SELECT语句将orders表中的最大order_id值存储到max_order_id变量中，并在没有任何匹配记录时将其设置为0。
初始化变量i、y、m、d的值，然后开始一个循环，重复执行以下步骤，直到i等于100：
a. 增加变量i的值。
b. 将变量m的值加1。如果m超过了12，则将其重置为1。
c. 将变量str设置为一个格式为'yyyy-MM-dd'的日期字符串。
d. 向orders表中插入一条记录，其中order_id为max_order_id+i，order_date为将变量str转换为日期格式后的值。
提交事务，完成数据插入操作。
总体来说，这段代码的作用是向orders表中插入100条记录，每条记录包含一个递增的order_id和一个递增的order_date，order_date的日期从2015年1月12日开始，每个月递增一天，直到100天后的日期。


```python
DECLARE
  i INTEGER;
  j INTEGER;
  max_order_id NUMBER;
BEGIN
  SELECT MAX(order_id) INTO max_order_id FROM orders;
  i := max_order_id - 99;
  WHILE i <= max_order_id LOOP
    FOR j IN 1..5 LOOP
      INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
      VALUES (SEQ1.nextval, i, 'product_' || j, j, j*100);
    END LOOP;
    i := i + 1;
  END LOOP;
  COMMIT;
END;

```

这段代码是一个PL/SQL块，用于向订单详情表 order_details 中插入数据。具体来说，它的主要过程如下：

查询订单表 orders 中最大的订单编号 max_order_id。

使用一个循环，从 max_order_id - 99 开始向订单详情表中插入 100 个订单的订单详情。

对于每个订单，使用另一个循环向订单详情表中插入 5 个商品的记录。这些记录包括一个自增的 id，一个对应的订单编号 i，以及商品的编号、数量和单价。

每插入完一个订单的所有商品记录，将订单的编号 i 增加 1，然后进入下一个订单的循环。

提交事务，完成插入操作。

值得注意的是，在这个代码块中，还使用了一个名为 SEQ1 的序列来为订单详情表中的 id 字段生成自增的值。这个序列在代码块之外定义，可能是使用 CREATE SEQUENCE 命令创建的。


```python
SELECT o.order_id, o.customer_name, o.order_date, d.product_id, d.product_num, d.product_price
FROM orders o
JOIN order_details d
ON o.order_id = d.order_id;
```

![](5.png)
![](5_1.png)

该 SQL 查询语句用于查询 orders 和 order_details 两个表中的数据，其中 orders 表中的 order_id 列和 order_details 表中的 order_id 列关联。查询的结果包括了以下列：

o.order_id: 订单编号，来自 orders 表
o.customer_name: 客户名称，来自 orders 表
o.order_date: 订单日期，来自 orders 表
d.product_id: 产品编号，来自 order_details 表
d.product_num: 产品数量，来自 order_details 表
d.product_price: 产品价格，来自 order_details 表
结果集将显示所有匹配的行，其中每一行都包括了这些列的值。如果一个订单中包含了多个产品，那么该订单会在结果集中出现多次，每次对应一个不同的产品。


```python
| Id  | Operation                           | Name          | Rows  | Bytes | Cost (%CPU)| Time     | Pstart| Pstop |
|   0 | SELECT STATEMENT                    |               |     1 |   105 |   275   (0)| 00:00:01 |       |       |
|   1 |  NESTED LOOPS                       |               |     1 |   105 |   275   (0)| 00:00:01 |       |       |
|   2 |   NESTED LOOPS                      |               |     1 |   105 |   275   (0)| 00:00:01 |       |       |
|   3 |    PARTITION REFERENCE ALL          |               |     1 |    61 |   274   (0)| 00:00:01 |     1 |     4 |
|   4 |     TABLE ACCESS FULL               | ORDER_DETAILS |     1 |    61 |   274   (0)| 00:00:01 |     1 |     4 |
|*  5 |    INDEX UNIQUE SCAN                | ORDERS_PK     |     1 |       |     0   (0)| 00:00:01 |       |       |
|   6 |   TABLE ACCESS BY GLOBAL INDEX ROWID| ORDERS        |     1 |    44 |     1   (0)| 00:00:01 | ROWID | ROWID |
Predicate Information (identified by operation id):
   5 - access("O"."ORDER_ID"="D"."ORDER_ID")
```

执行分析：该执行计划的每一行表示一个操作，以下是每一行的意义：

第一行表示 SELECT 语句的主操作，输出结果集。

第二行表示执行嵌套循环连接操作，将 orders 表与 order_details 表连接起来。

第三行表示执行嵌套循环连接操作，将 orders 表与 order_details 表连接起来。

第四行表示对 order_details 表进行全表扫描，这是第一个输入源。

第五行表示使用唯一索引 ORDERS_PK 在 orders 表中定位符合条件的行，这是第二个输入源。

第六行表示对 orders 表进行全局索引唯一扫描，输出结果行。

在这个执行计划中，我们可以看到执行连接操作的两个输入源，以及使用索引的方式。还可以看到每个操作的代价，即所需的 CPU 周期和内存使用情况。在这个执行计划中，我们可以看到最大的代价是全表扫描，代价是 274，因此我们应该尝试优化查询以减少全表扫描的次数，从而提高查询性能。

## 结论

上面的实验是建立一个分区表的过程。分区是Oracle中一种管理和组织表数据的方法，可以将大型表分成较小、更可管理的部分，可以提高查询效率、降低维护成本和优化数据管理。在这个实验中，我们创建了一个包含两个分区的表，一个分区包含2019年的数据，另一个包含2020年的数据。这个表的分区依据是order_date字段的值，order_date字段被指定为分区键。

在实际的应用中，可以根据实际需求选择不同的分区策略，如按照时间、地理位置、客户等等来进行分区。使用分区表需要注意的是，需要维护分区键和分区边界，并且一些操作（如增加、删除、修改分区键）需要特殊的语法和处理。因此，在使用分区表时，需要仔细考虑其适用场景，并且充分了解其原理和维护方法。


```python

```
