
-- 创建商品表
CREATE TABLE products (
  id       NUMBER,
  name     VARCHAR2(100),
  price    NUMBER(10, 2),
  category VARCHAR2(50),
  PRIMARY KEY (id)
) TABLESPACE data_ts;

-- 创建客户表
CREATE TABLE customers (
  id         NUMBER,
  name       VARCHAR2(100),
  address    VARCHAR2(200),
  phone      VARCHAR2(20),
  credit_lim NUMBER(10, 2),
  PRIMARY KEY (id)
) TABLESPACE data_ts;

-- 创建订单表
CREATE TABLE orders (
  id          NUMBER,
  customer_id NUMBER,
  order_date  DATE,
  total       NUMBER(10, 2),
  PRIMARY KEY (id),
  FOREIGN KEY (customer_id) REFERENCES customers (id)
) TABLESPACE data_ts;

-- 创建销售记录表
CREATE TABLE sales_records (
  id         NUMBER,
  order_id   NUMBER,
  product_id NUMBER,
  quantity   NUMBER,
  PRIMARY KEY (id),
  FOREIGN KEY (order_id) REFERENCES orders (id),
  FOREIGN KEY (product_id) REFERENCES products (id)
) TABLESPACE data_ts;

-- 删除表
DROP TABLE sales_records;
DROP TABLE orders;
DROP TABLE customers;
DROP TABLE products;


-- 插入2.5万条客户数据
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO customers (id, name, address, phone, credit_lim)
    VALUES (i, 'Customer ' || i, 'Address ' || i, 'Phone ' || i, ROUND(DBMS_RANDOM.VALUE(1000, 10000), 2));
  END LOOP;
  COMMIT;
END;
/

-- 插入2.5万条商品数据
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO products (id, name, price, category)
    VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 1000), 2), 'Category ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 插入2.5万条订单数据
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO orders (id, customer_id, order_date, total)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 25000)), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365)), ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
  END LOOP;
  COMMIT;
END;
/

-- 插入2.5万条销售记录数据
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO sales_records (id, order_id, product_id, quantity)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 25000)), ROUND(DBMS_RANDOM.VALUE(1, 25000)), ROUND(DBMS_RANDOM.VALUE(1, 10)));
  END LOOP;
  COMMIT;
END;
/


-- 检查客户表的数据行数
SELECT COUNT(*) FROM customers;

-- 检查商品表的数据行数
SELECT COUNT(*) FROM products;

-- 检查订单表的数据行数
SELECT COUNT(*) FROM orders;

-- 检查销售记录表的数据行数
SELECT COUNT(*) FROM sales_records;

GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sales_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sales_user;
GRANT SELECT ON customers TO sales_user;
GRANT SELECT ON sales_records TO sales_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON products TO inventory_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales_records TO inventory_user;

-- 查询用户
SELECT username FROM all_users;

-- 查询用户权限
SELECT * FROM user_sys_privs WHERE username = 'SALES_USER';
-- 查询用户的表权限
SELECT * FROM user_tab_privs WHERE grantee = 'SALES_USER';


-- 删除 sales_package 程序包
DROP PACKAGE sales_package;



///////////////////////////////////////222222222



CREATE OR REPLACE PACKAGE sales_package AS
  -- 存储过程：计算指定客户的购买历史
  PROCEDURE get_customer_purchase_history(
    customer_id IN NUMBER,
    purchase_history OUT SYS_REFCURSOR
  );

  -- 存储过程：获取最畅销的产品
  PROCEDURE get_best_selling_product(
    best_selling_product OUT SYS_REFCURSOR
  );

  -- 存储过程：计算每个客户的总购买金额
  PROCEDURE get_total_purchase_amount_per_customer(
    total_purchase_amount OUT SYS_REFCURSOR
  );

  -- 存储过程：计算指定产品的销售数量
  PROCEDURE get_product_sales_quantity(
    product_id IN NUMBER,
    sales_quantity OUT NUMBER
  );

  -- 存储过程：计算每个客户的平均购买金额
  PROCEDURE get_average_purchase_amount_per_customer(
    average_purchase_amount OUT SYS_REFCURSOR
  );
END sales_package;
/

CREATE OR REPLACE PACKAGE BODY sales_package AS
  -- 存储过程：计算指定客户的购买历史
  PROCEDURE get_customer_purchase_history(
    customer_id IN NUMBER,
    purchase_history OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN purchase_history FOR
      SELECT o.id, p.name, s.quantity, p.price, s.quantity * p.price AS total_amount
      FROM orders o
      JOIN sales_records s ON o.id = s.order_id
      JOIN products p ON s.product_id = p.id
      WHERE o.customer_id = customer_id;
  END get_customer_purchase_history;

  -- 存储过程：获取最畅销的产品
  PROCEDURE get_best_selling_product(
    best_selling_product OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN best_selling_product FOR
      SELECT p.id, p.name, SUM(s.quantity) AS total_quantity
      FROM sales_records s
      JOIN products p ON s.product_id = p.id
      GROUP BY p.id, p.name
      ORDER BY total_quantity DESC
      FETCH FIRST ROW ONLY;
  END get_best_selling_product;

  -- 存储过程：计算每个客户的总购买金额
  PROCEDURE get_total_purchase_amount_per_customer(
    total_purchase_amount OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN total_purchase_amount FOR
      SELECT c.id, c.name, SUM(s.quantity * p.price) AS total_amount
      FROM customers c
      JOIN orders o ON c.id = o.customer_id
      JOIN sales_records s ON o.id = s.order_id
      JOIN products p ON s.product_id = p.id
      GROUP BY c.id, c.name;
  END get_total_purchase_amount_per_customer;

  -- 存储过程：计算指定产品的销售数量
  PROCEDURE get_product_sales_quantity(
    product_id IN NUMBER,
    sales_quantity OUT NUMBER
  ) AS
  BEGIN
    SELECT SUM(quantity)
    INTO sales_quantity
    FROM sales_records
    WHERE product_id = product_id;
  END get_product_sales_quantity;

  -- 存储过程：计算每个客户的平均购买金额
  PROCEDURE get_average_purchase_amount_per_customer(
    average_purchase_amount OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN average_purchase_amount FOR
      SELECT c.id, c.name, AVG(s.quantity * p.price) AS average_amount
      FROM customers c
      JOIN orders o ON c.id = o.customer_id
      JOIN sales_records s ON o.id = s.order_id
      JOIN products p ON s.product_id = p.id
      GROUP BY c.id, c.name;
  END get_average_purchase_amount_per_customer;
END sales_package;
/







////////





-- 示例调用存储过程 get_best_selling_product
DECLARE
  best_selling_product SYS_REFCURSOR;
  product_id NUMBER;
  product_name VARCHAR2(100);
  total_quantity NUMBER;
BEGIN
  sales_package.get_best_selling_product(best_selling_product => best_selling_product);
  FETCH best_selling_product INTO product_id, product_name, total_quantity;
  IF best_selling_product%FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Best Selling Product: ' || product_name || ', Total Quantity: ' || total_quantity);
  END IF;
  CLOSE best_selling_product;
END;
/

-- 示例调用存储过程 get_total_purchase_amount_per_customer
DECLARE
  total_purchase_amount SYS_REFCURSOR;
  customer_id NUMBER;
  customer_name VARCHAR2(100);
  total_amount NUMBER;
BEGIN
  sales_package.get_total_purchase_amount_per_customer(total_purchase_amount => total_purchase_amount);
  LOOP
    FETCH total_purchase_amount INTO customer_id, customer_name, total_amount;
    EXIT WHEN total_purchase_amount%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('Customer ID: ' || customer_id || ', Customer Name: ' || customer_name || ', Total Purchase Amount: ' || total_amount);
  END LOOP;
  CLOSE total_purchase_amount;
END;
/



DECLARE
  purchase_history SYS_REFCURSOR;
  order_id NUMBER;
  product_name VARCHAR2(100);
  quantity NUMBER;
  price NUMBER;
  total_amount NUMBER;
  counter NUMBER := 0; -- 计数器
BEGIN
  sales_package.get_customer_purchase_history(customer_id => 1, purchase_history => purchase_history);
  LOOP
    FETCH purchase_history INTO order_id, product_name, quantity, price, total_amount;
    EXIT WHEN purchase_history%NOTFOUND OR counter >= 100; -- 添加计数器判断条件
    counter := counter + 1; -- 每次循环计数器加1
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || order_id || ', Product: ' || product_name || ', Quantity: ' || quantity || ', Price: ' || price || ', Total Amount: ' || total_amount);
  END LOOP;
  CLOSE purchase_history;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error occurred: ' || SQLERRM);
END;
/


DECLARE
  product_id NUMBER := 1; -- 替换为实际的产品ID
  sales_quantity NUMBER;
BEGIN
  sales_package.get_product_sales_quantity(product_id => product_id, sales_quantity => sales_quantity);
  DBMS_OUTPUT.PUT_LINE('Sales Quantity for Product ID ' || product_id || ': ' || sales_quantity);
END;



DECLARE
  average_purchase_amount SYS_REFCURSOR;
  customer_id NUMBER;
  customer_name VARCHAR2(100);
  average_amount NUMBER;
BEGIN
  sales_package.get_average_purchase_amount_per_customer(average_purchase_amount => average_purchase_amount);
  LOOP
    FETCH average_purchase_amount INTO customer_id, customer_name, average_amount;
    EXIT WHEN average_purchase_amount%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('Customer ID: ' || customer_id || ', Name: ' || customer_name || ', Average Purchase Amount: ' || average_amount);
  END LOOP;
  CLOSE average_purchase_amount;
END;


//////






